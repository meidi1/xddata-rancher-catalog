# 曦点数据 系统监控 

### 说明:
此模板创建、扩展一个多节点的系统信息监控集群（基于rancher环境）。通过配置生成适用于rancher环境的元数据.
 集群规模可在部署完成后根据需要横向扩容。

### rancher api授权信息获取:
在rancher页面点击API下拉菜单>选择"秘钥">"添加账号API Key">在"新建API Key"页面输入名称，然后
点击创建。添加成功后把对应的用户名（Access Key）和密码（Secret Key）进行保存，用于应用商店相关配置信息的填写。如果忘记了这两项内容，您需要重新创建新的API Key。

### 用法:

  从catalog中选择曦点数据 - 系统监控. 
 
 输入节点数量, 配置一下参数

 Change the following zookeeper default parameters, if you need:

- ZK_DATA_DIR="/opt/zk/data"
- ZK_INIT_LIMIT="10"
- ZK_MAX_CLIENT_CXNS="500"
- ZK_SYNC_LIMIT="5"
- ZK_TICK_TIME="2000"
- host_label=""                         # Host label where to deploy zookeeper.
 
 点击 创建.
 

 注意: 当你扩展集群或升级节点时, 系统将会先停止当前服务后完成扩展（升级），此过程服务不可用。
